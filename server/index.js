import express from 'express';
import logger from 'morgan';

import { Server } from 'socket.io';
import { createServer } from 'node:http';
import dotenv from 'dotenv'
// import { createClient } from '@libsql/client/.';
import { createConnection } from 'mysql';

dotenv.config();

const port = process.env.PORT ?? 3000;

const app = express();
const server = createServer(app);
const io = new Server(server, {
  connectionStateRecovery: {}
});

//.env
const conn = createConnection({
  host: process.env.DB_URL,
  database: process.env.DB_NAME,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
});

//sql
conn.connect(function (err) {
  if (err) {
    throw err;
  }

  console.log('connected as id ');

  conn.query('CREATE TABLE IF NOT EXISTS messages ( id INTEGER PRIMARY KEY AUTO_INCREMENT, content MEDIUMTEXT, user MEDIUMTEXT )', function (error, results, fields) {
    if (error) throw error;
  });
});

io.on('connection', (socket) => {
  console.log('a user has connected!')

  socket.on('disconnect', () => {
    console.log('a user has disconnected')
  })

  socket.on('chat message', async (msg) => {

    let username = await socket.handshake.auth.username  ?? 'anonymous';

    //inserta el mensaje en base de datos en "?"
    conn.query('INSERT INTO messages (content, user) VALUES (?, ?)', [msg, username], function (error, results, fields) {

      //emite el identificador, mensaje, id
      io.emit('chat message', msg, results.insertId.toString(), username); 

      if (error) throw error;
      // conectao!
    });
  });

  //recuperar mensajes
  if (!socket.recovered) {

    //documentacion
    conn.query('SELECT * FROM messages where id > ?', [socket.handshake.auth.serverOffset ?? 0], function (error, results, fields) {

      // console.log(results);
      results.forEach(row => {
        // console.log(row.content);
        socket.emit('chat message', row.content, row.id.toString(), row.user);

      });

      if (error) throw error;
      // conectao!
    });
  }
});

app.use(logger('dev'));

app.get('/', (req, res) => {
  res.sendFile(process.cwd() + '/client/index.html')
});

app.get('*', function(req, res){
  res.send('what???', 404);
});

server.listen(port, () => {
  console.log(`server running on port ${port}`)
});